package mss.codingtest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    JSONObject jsonResponse;
    ListView list_results;
    EditText text_input;
    ArrayList<ListItem> listItemList=new ArrayList<ListItem>();
    RequestQueue queue;
    ImageView dialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_input   = (EditText)findViewById(R.id.input);
        list_results   = (ListView)findViewById(R.id.results);

        queue = Volley.newRequestQueue(this);

        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        list_results.setAdapter(adapter);
        list_results.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                Toast toast = Toast.makeText(getApplicationContext(), listItemList.get(position).src, Toast.LENGTH_SHORT);
                toast.show();
                try {
                    showDialogue(listItemList.get(position).src);
                } catch (Exception e) {
                    Log.e("Imageload", "error getting image");
                }
            }
        });



    }





    public void showDialogue(String src){

        final String srcString = src;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);



        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                dialogView = (ImageView) dialogLayout.findViewById(R.id.dialog_image);
                getImage(srcString, dialogView);

        }});

        dialog.show();
}




    public void addItem(ListItem item) {
        listItemList.add(item);
        adapter.add(item.title + ", width:" + item.width + ", height:" + item.height);
    }


    protected void doSearch(View view){
        ArrayList<String> listItems=new ArrayList<String>();
        ArrayList<ListItem> listItemList=new ArrayList<ListItem>();
        adapter.clear();


        String searchString = text_input.getText().toString();
        Toast toast = Toast.makeText(getApplicationContext(), "Searching for "+searchString, Toast.LENGTH_SHORT);
        toast.show();

        String url ="http://api.flickr.com/services/feeds/photos_public.gne?nojsoncallback=1&tagmode=any&format=json&tags="+searchString;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonResponse = new JSONObject(response);
                            for(int i = 0; i < jsonResponse.getJSONArray("items").length(); i++){

                                JSONArray items = jsonResponse.getJSONArray("items");
                                JSONObject title = (JSONObject) items.get(i);
                                String rowTitle = title.get("title").toString();
                                String htmlDescription = title.get("description").toString();
                                Document description = Jsoup.parse(htmlDescription);

                                String height = description.select("img").first().attr("width").toString();
                                String width = description.select("img").first().attr("height").toString();
                                String src = description.select("img").first().attr("src").toString();
                                ListItem item = new ListItem(height, width, rowTitle, src);
                                addItem(item);

                            }
                        } catch (JSONException e) {
                            Log.e("JSON", "Error parsing " + e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getApplicationContext(), "There was an error.", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        queue.add(stringRequest);


    }

    private void getImage(String url, ImageView imageView){
        ImageRequestHandler.imageRequest(url, this, imageView);
    }


}
