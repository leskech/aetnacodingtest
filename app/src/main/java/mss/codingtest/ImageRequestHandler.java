package mss.codingtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by mike on 5/11/2017.
 */

public class ImageRequestHandler {

        public static void imageRequest(String url, Context context, final ImageView imageView) {
            RequestQueue queue = Volley.newRequestQueue(context);
            ImageRequest imageReq = new ImageRequest(url,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            imageView.setImageBitmap(response);
                        }
                    }, 0, 0, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("image load","error loading image");
                }
            });
            queue.add(imageReq);
        }



}
