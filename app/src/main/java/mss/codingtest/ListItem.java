package mss.codingtest;

/**
 * Created by mike on 5/11/2017.
 */

public class ListItem {
    public String height;
    public String width;
    public String title;
    public String src;

    public ListItem(String height, String width, String title, String src){
        this.height = height;
        this.width = width;
        this.title = title;
        this.src = src;
    }


}
